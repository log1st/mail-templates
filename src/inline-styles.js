var glob = require("glob");
var fs = require("fs");
var inlineCss = require('inline-css');

const IMAGES_ROOT = process.env.IMAGES_ROOT || "";

console.log(`${IMAGES_ROOT}/_nuxt`);

glob('dist/templates/**/*.html', {}, function (er, files) {
  files.filter(file => file.match(/\/dist\/templates\//));

  files.map(file => {
    fs.readFile(file, 'utf8', function (err, content) {
      inlineCss(content, {
        url: '/',
        preserveMediaQueries: true,
      })
        .then(html => {
          const matches = html.match(/.*?(<table.*\/table>).*/s);

          const result = String(matches ? matches[1] : html);

          const resultWithImages = result.replace(/\/_nuxt/g, `${IMAGES_ROOT}/_nuxt`);

          fs.writeFile(file, resultWithImages, function(err) {})
        })
    })
  })
});
