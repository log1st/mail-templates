const webpack = require('webpack')

module.exports = {
  css: [
    "~assets/assets.scss"
  ],
  head: {
    title: 'Email template',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { 'http-equiv': 'Content-Type', content: 'text/html charset=UTF-8'}
    ],
  },
  build: {
    open: process.env.NODE_ENV === 'development',
    babel: {
      plugins: ['syntax-jsx', 'transform-vue-jsx']
    },
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
      config.module.rules.find(item => item.test.source === /\.css$/.source).oneOf = [({
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: `${isDev ? '[local]_' : ''}[hash:base64:8]`,
              camelCase: true,
            }
          }
        ]
      })];

      config.plugins.push(new webpack.LoaderOptionsPlugin({
        options: {
          context: process.cwd() // or the same value as `context`
        }
      }))

      config.plugins.push(new webpack.DefinePlugin({
        IMAGES_ROOT: JSON.stringify(process.env.NODE_ENV || '')
      }))

      config.module.rules.map((rule) => {
        if(rule.test.source.match(/jpe\?g/)) {
          rule.use[0].options = {
            ...rule.use[0].options,
            limit: 1,
          }
        }

        return rule
      })
    }
  },
  generate: {
    routes: () => [
      '/',
    ]
  },
};
